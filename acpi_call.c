/*-
 *   Copyright (C) 2018 Intel Corporation
 *   Copyright (C) 2011 by Maxim Ignatenko
 *   gelraen.ua@gmail.com
 *
 *   All rights reserved.                                                  *
 *                                                                         *
 *   Redistribution and use in source and binary forms, with or without    *
 *    modification, are permitted provided that the following conditions   *
 *    are met:                                                             *
 *     * Redistributions of source code must retain the above copyright    *
 *       notice, this list of conditions and the following disclaimer.     *
 *     * Redistributions in binary form must reproduce the above copyright *
 *       notice, this list of conditions and the following disclaimer in   *
 *       the documentation and/or other materials provided with the        *
 *       distribution.                                                     *
 *                                                                         *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS   *
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT     *
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR *
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  *
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, *
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT      *
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, *
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY *
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT   *
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE *
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  *
 *
 */

#include <sys/types.h>
#include <sys/module.h>
#include <sys/systm.h>
#include <sys/errno.h>
#include <sys/param.h>
#include <sys/kernel.h>
#if __FreeBSD__ >= 8
#	include <contrib/dev/acpica/include/acpi.h>
#else
#	include <contrib/dev/acpica/acpi.h>
#endif
#include <dev/acpica/acpiio.h>
#include "acpi_call_io.h"


void acpi_call_fixup_pointers(ACPI_OBJECT *p, UINT8 *orig);

static void
free_args(ACPI_OBJECT_LIST *args)
{
	ACPI_OBJECT_LIST	*tmp;
	int			 i;

	if (args == NULL)
		return;

	for (i = 0; i < args->Count; i++) {
		switch (args->Pointer[i].Type) {
		case ACPI_TYPE_STRING:
			AcpiOsFree(args->Pointer[i].String.Pointer);
			break;
		case ACPI_TYPE_BUFFER:
			AcpiOsFree(args->Pointer[i].Buffer.Pointer);
			break;
		case ACPI_TYPE_PACKAGE:
			tmp = (ACPI_OBJECT_LIST *)args->Pointer[i].Package.Elements;
			free_args(tmp - 1);
		default:
			break;
		}
	}
	AcpiOsFree(args);
}

static ACPI_OBJECT_LIST *
copyin_args(ACPI_OBJECT_LIST *src)
{
	ACPI_OBJECT_LIST	 tmp_list;
	ACPI_OBJECT_LIST	*dest;
	void			*v;
	uint32_t		 i;
	bool			 failed;

	if (src->Count > 7)
		return (NULL);

	dest = AcpiOsAllocate(sizeof(ACPI_OBJECT_LIST) + sizeof(ACPI_OBJECT) * src->Count);
	if (dest == NULL)
		return (NULL);

	dest->Count = src->Count;
	dest->Pointer = (ACPI_OBJECT *)(dest + 1);

	if (copyin(src->Pointer, dest->Pointer, sizeof(ACPI_OBJECT) * dest->Count) != 0) {
		AcpiOsFree(dest);
		return (NULL);
	}

	failed = false;

	for (i = 0; i < dest->Count; i++) {
		switch (dest->Pointer[i].Type) {
		case ACPI_TYPE_INTEGER:
			break;
		case ACPI_TYPE_STRING:
			v = AcpiOsAllocate(dest->Pointer[i].String.Length);
			if (v == NULL || copyin(dest->Pointer[i].String.Pointer, v, dest->Pointer[i].String.Length) != 0)
				failed = true;
			dest->Pointer[i].String.Pointer = v;
			break;
		case ACPI_TYPE_BUFFER:
			v = AcpiOsAllocate(dest->Pointer[i].Buffer.Length);
			if (v == NULL || copyin(dest->Pointer[i].Buffer.Pointer, v, dest->Pointer[i].Buffer.Length) != 0)
				failed = true;
			dest->Pointer[i].String.Pointer = v;
			break;
		case ACPI_TYPE_PACKAGE:
			tmp_list.Count = dest->Pointer[i].Package.Count;
			tmp_list.Pointer = dest->Pointer[i].Package.Elements;
			v = copyin_args(&tmp_list);
			if (v == NULL)
				failed = true;
			dest->Pointer[i].Package.Elements = ((ACPI_OBJECT_LIST *)v)->Pointer;
			break;
		default:
			failed = true;
			break;
		}
	}

	if (failed) {
		free_args(dest);
		dest = NULL;
	}

	return (dest);
}

static int
acpi_call_ioctl(u_long cmd, caddr_t addr, void *arg)
{
	char			 path[256];
	ACPI_BUFFER		 result;
	ACPI_OBJECT_LIST	*args;
	struct acpi_call_descr	*params;

	result.Length = ACPI_ALLOCATE_BUFFER;
	result.Pointer = NULL;

	if (cmd != ACPIIO_CALL)
		return (ENXIO);

	params = (struct acpi_call_descr*)addr;

	if (copyinstr(params->path, path, sizeof(path), NULL))
		return (EINVAL);

	args = copyin_args(&params->args);
	if (args == NULL)
		return (EINVAL);

	params->retval = AcpiEvaluateObject(NULL, path, &params->args, &result);
	free_args(args);

	if (!ACPI_SUCCESS(params->retval) || result.Pointer == NULL) {
		AcpiOsFree(result.Pointer);
		return (EINVAL);
	}

	if (params->result.Pointer != NULL)
	{
		params->result.Length = min(params->result.Length, result.Length);
		if (result.Length >= sizeof(ACPI_OBJECT))
			acpi_call_fixup_pointers(result.Pointer, params->result.Pointer);
		copyout(result.Pointer, params->result.Pointer, params->result.Length);
		params->reslen = result.Length;
	}
	AcpiOsFree(result.Pointer);

	return (0);
}

void
acpi_call_fixup_pointers(ACPI_OBJECT *p, UINT8 *dest)
{
	switch (p->Type)
	{
	case ACPI_TYPE_STRING:
		p->String.Pointer += (char *)dest - (char *)p;
		break;
	case ACPI_TYPE_BUFFER:
		p->Buffer.Pointer += (char *)dest - (char *)p;
		break;
	}
}

static int
acpi_call_loader(struct module *m, int what, void *arg)
{
	int err = 0;

   	switch (what) {
	case MOD_LOAD:
		err = acpi_register_ioctl(ACPIIO_CALL, acpi_call_ioctl, NULL);
		break;
	case MOD_UNLOAD:
		acpi_deregister_ioctl(ACPIIO_CALL, acpi_call_ioctl);
		break;
	default:
		err = EOPNOTSUPP;
		break;
	}
	return(err);
}

static moduledata_t acpi_call_mod = {
	"acpi_call",
	acpi_call_loader,
	NULL
};

DECLARE_MODULE(acpi_call, acpi_call_mod, SI_SUB_EXEC, SI_ORDER_ANY);
MODULE_DEPEND(acpi_call, acpi, 1, 1, 1);
